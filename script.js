let urlStandingsPL = "https://api.football-data.org/v2/competitions/2021/standings?standingType=TOTAL";

fetch(urlStandingsPL, {
    method:"GET",
    
    headers: {
        "x-auth-token": "3eaab6b3b0064537a9270949dfd7658b"
    }
})

.then(response => response.json())
.then(function (data) {
    let html="";
    data.standings[0].table.forEach(element => {
        html += "<tr>" + "<th><img src='" + element.team.crestUrl + "'/>" + element.team.name + "</th>" + "<td>" + element.position + "</td>" + "<td>" + element.points + "</td>" + "<td>" + element.won + "</td>" + "<td>" + element.goalsFor + "</td>" + "</tr>";
    });
    
document.getElementById("TabellenInhalt").innerHTML = html;
});

let urlStandingsCL = "https://api.football-data.org/v2/competitions/2001/standings?standingType=TOTAL";

/*Gruppe*/
fetch(urlStandingsCL, {
    method:"GET",
    
    headers: {
        "x-auth-token": "3eaab6b3b0064537a9270949dfd7658b"
    }
})
.then(response => response.json())
.then(function (data) {
    let htmlA="";
    
        data.standings[0].table.forEach(element => {
            htmlA += "<tr>" + "<th><img src='" + element.team.crestUrl + "'/>" + element.team.name + "</th>" + "<td>" + element.position + "</td>" + "<td>" + element.points + "</td>" + "<td>" + element.won + "</td>" + "<td>" + element.goalsFor + "</td>" + "</tr>";
        });
        document.getElementById("TabellenInhaltCLA").innerHTML = htmlA;
    
    let htmlB="";
        data.standings[1].table.forEach(element => {
            htmlB += "<tr>" + "<th><img src='" + element.team.crestUrl + "'/>" + element.team.name + "</th>" + "<td>" + element.position + "</td>" + "<td>" + element.points + "</td>" + "<td>" + element.won + "</td>" + "<td>" + element.goalsFor + "</td>" + "</tr>";
        });
        document.getElementById("TabellenInhaltCLB").innerHTML = htmlB;

    let htmlC="";
        data.standings[2].table.forEach(element => {
            htmlC += "<tr>" + "<th><img src='" + element.team.crestUrl + "'/>" + element.team.name + "</th>" + "<td>" + element.position + "</td>" + "<td>" + element.points + "</td>" + "<td>" + element.won + "</td>" + "<td>" + element.goalsFor + "</td>" + "</tr>";
        });
        document.getElementById("TabellenInhaltCLC").innerHTML = htmlC;

    let htmlD="";
        data.standings[3].table.forEach(element => {
            htmlD += "<tr>" + "<th><img src='" + element.team.crestUrl + "'/>" + element.team.name + "</th>" + "<td>" + element.position + "</td>" + "<td>" + element.points + "</td>" + "<td>" + element.won + "</td>" + "<td>" + element.goalsFor + "</td>" + "</tr>";
        });
        document.getElementById("TabellenInhaltCLD").innerHTML = htmlD;

    let htmlE="";
        data.standings[4].table.forEach(element => {
            htmlE += "<tr>" + "<th><img src='" + element.team.crestUrl + "'/>" + element.team.name + "</th>" + "<td>" + element.position + "</td>" + "<td>" + element.points + "</td>" + "<td>" + element.won + "</td>" + "<td>" + element.goalsFor + "</td>" + "</tr>";
        });
        document.getElementById("TabellenInhaltCLE").innerHTML = htmlE;

    let htmlF="";
        data.standings[5].table.forEach(element => {
            htmlF += "<tr>" + "<th><img src='" + element.team.crestUrl + "'/>" + element.team.name + "</th>" + "<td>" + element.position + "</td>" + "<td>" + element.points + "</td>" + "<td>" + element.won + "</td>" + "<td>" + element.goalsFor + "</td>" + "</tr>";
        });
        document.getElementById("TabellenInhaltCLF").innerHTML = htmlF;

    let htmlG="";
        data.standings[6].table.forEach(element => {
            htmlG += "<tr>" + "<th><img src='" + element.team.crestUrl + "'/>" + element.team.name + "</th>" + "<td>" + element.position + "</td>" + "<td>" + element.points + "</td>" + "<td>" + element.won + "</td>" + "<td>" + element.goalsFor + "</td>" + "</tr>";
        });
        document.getElementById("TabellenInhaltCLG").innerHTML = htmlG;

    let htmlH="";
        data.standings[7].table.forEach(element => {
            htmlH += "<tr>" + "<th><img src='" + element.team.crestUrl + "'/>" + element.team.name + "</th>" + "<td>" + element.position + "</td>" + "<td>" + element.points + "</td>" + "<td>" + element.won + "</td>" + "<td>" + element.goalsFor + "</td>" + "</tr>";
        });
        document.getElementById("TabellenInhaltCLH").innerHTML = htmlH;
});




function PL() {
    document.getElementsByClassName("PremierLeague")[0].style.display = "flex";

    document.getElementsByClassName("Buttons")[0].style.display = "none";

    document.getElementsByClassName("Zurückbuttons")[0].style.display = "flex";
    
}

function CL() {
    document.getElementsByClassName("ChampionsLeague")[0].style.display = "grid";

    document.getElementsByClassName("Buttons")[0].style.display = "none";

    document.getElementsByClassName("Zurückbuttons")[0].style.display = "flex";
}

function Zurück() {
    document.getElementsByClassName("Zurückbuttons")[0].style.display = "none";

    document.getElementsByClassName("Buttons")[0].style.display = "flex";

    document.getElementsByClassName("PremierLeague")[0].style.display = "none";

    document.getElementsByClassName("ChampionsLeague")[0].style.display = "none";
}